﻿namespace Wacton.Japangolin.Tests;

using NUnit.Framework;
using Wacton.Japangolin.Core.Conjugation;
using Wacton.Japangolin.Core.Enums;
using Wacton.Japangolin.Core.Words;

public class Adjectiveい
{
    private readonly Word word = new() { Kanji = "美味しい", Kana = "おいしい", Class = WordClass.AdjectiveI };

    [Test]
    public void Dictionary()
    {
        var (kana, kanji) = Inflection.Dictionary.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しい"));
        Assert.That(kana, Is.EqualTo("おいしい"));
    }

    [Test]
    public void Stem()
    {
        var (kana, kanji) = Inflection.Stem.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味し"));
        Assert.That(kana, Is.EqualTo("おいし"));
    }

    [Test]
    public void Te()
    {
        var (kana, kanji) = Inflection.Te.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しくて"));
        Assert.That(kana, Is.EqualTo("おいしくて"));
    }

    [Test]
    public void PresentAffirmativeLong()
    {
        var (kana, kanji) = Inflection.PresentAffirmativeLong.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しいです"));
        Assert.That(kana, Is.EqualTo("おいしいです"));
    }

    [Test]
    public void PresentAffirmativeShort()
    {
        var (kana, kanji) = Inflection.PresentAffirmativeShort.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しい"));
        Assert.That(kana, Is.EqualTo("おいしい"));
    }

    [Test]
    public void PresentNegativeLong()
    {
        var (kana, kanji) = Inflection.PresentNegativeLong.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しくないです"));
        Assert.That(kana, Is.EqualTo("おいしくないです"));
    }

    [Test]
    public void PresentNegativeShort()
    {
        var (kana, kanji) = Inflection.PresentNegativeShort.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しくない"));
        Assert.That(kana, Is.EqualTo("おいしくない"));
    }

    [Test]
    public void PastAffirmativeLong()
    {
        var (kana, kanji) = Inflection.PastAffirmativeLong.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しかったです"));
        Assert.That(kana, Is.EqualTo("おいしかったです"));
    }

    [Test]
    public void PastAffirmativeShort()
    {
        var (kana, kanji) = Inflection.PastAffirmativeShort.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しかった"));
        Assert.That(kana, Is.EqualTo("おいしかった"));
    }

    [Test]
    public void PastNegativeLong()
    {
        var (kana, kanji) = Inflection.PastNegativeLong.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しくなかったです"));
        Assert.That(kana, Is.EqualTo("おいしくなかったです"));
    }

    [Test]
    public void PastNegativeShort()
    {
        var (kana, kanji) = Inflection.PastNegativeShort.Conjugate(word);
        Assert.That(kanji, Is.EqualTo("美味しくなかった"));
        Assert.That(kana, Is.EqualTo("おいしくなかった"));
    }
}