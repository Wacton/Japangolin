﻿namespace Wacton.Japangolin.Desktop.Mains;

using Wacton.Japangolin.Desktop.MVVM;

public class NoDetailViewModel : DetailViewModel
{
    public NoDetailViewModel(ModelWatcher modelWatcher) : base(modelWatcher)
    {
    }
}