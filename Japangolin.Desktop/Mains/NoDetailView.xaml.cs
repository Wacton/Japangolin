﻿namespace Wacton.Japangolin.Desktop.Mains;

using System.Windows.Controls;

/// <summary>
/// Interaction logic for NoDetailView.xaml
/// </summary>
public partial class NoDetailView : UserControl
{
    public NoDetailView()
    {
        InitializeComponent();
    }
}