﻿namespace Wacton.Japangolin.Core.Enums;

public enum WordClass
{
    Unknown,
    Noun,
    AdjectiveNa,
    AdjectiveI,
    VerbRu,
    VerbU
}